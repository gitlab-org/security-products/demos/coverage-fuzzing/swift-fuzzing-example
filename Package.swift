// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "example-swift",
    dependencies: [
	.package(url: "https://github.com/swift-server/swift-backtrace.git", .revision("4915cdd24e3300065cda228cb50d35ba00094aa3"))
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "example-swift",
            dependencies: ["Backtrace"]),
        .testTarget(
            name: "example-swiftTests",
            dependencies: ["example-swift"]),
    ]
)
